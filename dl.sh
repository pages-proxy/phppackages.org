dl()
{(
  echo $1
  install -d $1
  cd $1
  curl --remote-name --remote-header-name --silent https://phppackages.org/p/$1/badge/rank.svg # | gzip -9 > rank.svgz
  file rank.svg
  curl --remote-name --remote-header-name --silent https://phppackages.org/p/$1/badge/referenced-by.svg # | gzip -9 > referenced-by.svgz
  file referenced-by.svg

)}
